local wezterm = require("wezterm")

local config = wezterm.config_builder()

config.color_scheme = "kanagawa (Gogh)"
config.enable_tab_bar = false
config.font = wezterm.font("JetBrains Mono")
config.font_size = 14
config.audible_bell = "Disabled"
config.window_background_opacity = 0.90

return config
